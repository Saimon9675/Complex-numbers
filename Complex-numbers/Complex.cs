using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Complex_numbers
{
    public class Complex
    {
        double real;
        double imagin;

        public Complex(double r, double im)
        {
            real = r;
            imagin = im;
        }        

        public double Real { get => real; set => real = value; }
        public double Imagin { get => imagin; set => imagin = value; }
    }   
    
    public partial class Program
    {
        public static bool AreEqualComplexNumbers(Complex a, Complex b)
        {
            if ((a.Real == b.Real) && (a.Imagin == b.Imagin))
                return true;
            else
                return false;
        }

        public static Complex EnteredValue()
        {
            double r = 0, im = 0;
            double k;
            bool isOk = false;
            do
            {
                Console.WriteLine("������� �������������� ����� �����");
                string s = Console.ReadLine();
                if (Double.TryParse(s, out k))
                {
                    r = k;
                    isOk = true;
                }
                else
                    Console.WriteLine("���������� ������ �����");
            } while (isOk == false);

            isOk = false;
            do
            {
                Console.WriteLine("������� ������ ����� �����");
                string s = Console.ReadLine();
                if (Double.TryParse(s, out k))
                {
                    im = k;
                    isOk = true;
                }
                else
                    Console.WriteLine("���������� ������ �����");
            } while (isOk == false);
            
            return new Complex(r, im);
        }        

        public static Complex SumComplex(Complex a, Complex b)
        {
            return (new Complex(a.Real + b.Real, a.Imagin + b.Imagin));
        }

        public static Complex DifComplex(Complex a, Complex b)
        {
            return (new Complex(a.Real - b.Real, a.Imagin - b.Imagin));
        }

        public static Complex MultiplyingComplex(Complex a, Complex b)
        {
            double real = a.Real * b.Real - a.Imagin * b.Imagin;
            double imagin = a.Real * b.Imagin + a.Imagin * b.Real;
            return new Complex(real, imagin);
        }


        public static Complex PowComplex(Complex a, int degree)
        {
            Complex res = a;
            for (int i = 1; i < degree; i++)
            {
                res = MultiplyingComplex(res, a);
            }
            return res;
        }

        public static Complex[] RootComplex(Complex a, int degree)
        {
            Complex[] res = new Complex[degree];

            double z = Math.Sqrt(a.Real * a.Real + a.Imagin * a.Imagin);
            double phi = Math.Atan2(a.Imagin, a.Real);
            z = Math.Pow(z, 1 / (double)degree);
            if (a.Real == 0)
                phi += Math.PI;

            for (int i = 0; i < degree; i++)
            {
                double r = z * Math.Cos((phi + 2 * Math.PI * i) / degree);
                double im = z * Math.Sin((phi + 2 * Math.PI * i) / degree);
                res[i] = new Complex(r, im);
            }

            return res;
        }
        
        public static Complex DividingComplex(Complex a, Complex b)
        {
            double real = (a.Real * b.Real + a.Imagin * b.Imagin) / (b.Real * b.Real + b.Imagin * b.Imagin);
            double imagin = (b.Real * a.Imagin - a.Real * b.Imagin) / (b.Real * b.Real + b.Imagin * b.Imagin);  
            if (b.Real == 0)
            {
                real *= -1;
                imagin *= -1;
            }
            return new Complex(real, imagin);
        }                

        public static String AlgebToTrigComplex(Complex a)
        {
            String s = " ";
            double z = Math.Sqrt(a.Real * a.Real + a.Imagin * a.Imagin);
            double phi = Math.Atan2(a.Imagin, a.Real);
            if (phi < 0)
                phi += 2 * Math.PI;
            phi = Math.Round(phi, 3);
            if (a.Real == 0)
                phi += Math.PI;
            
            s = "cos(" + (phi*z).ToString() + ") + sin(" + (phi*z).ToString() + ")i";
            return s;
        }

        public static void PrintComplex(Complex a)
        {
            String s = Convert.ToString(a.Real);
            if (a.Real != 0)
            {
                if (a.Imagin > 0)
                    s += " + " + Convert.ToString(a.Imagin) + "i";
                else if (a.Imagin < 0)
                    s += " - " + Convert.ToString(a.Imagin * -1) + "i";
            }
            else
                s = Convert.ToString(a.Imagin) + "i";
            Console.WriteLine(s);
            Console.ReadLine();
        }

        public static void PrintComplex(Complex[] a, int count)
        {
            for (int i = 0; i < count; i++)
            {
                String s = Convert.ToString(a[i].Real);
                if (a[i].Real != 0)
                {
                    if (a[i].Imagin > 0)
                        s += " + " + Convert.ToString(a[i].Imagin) + "i";
                    else if (a[i].Imagin < 0)
                        s += " - " + Convert.ToString(a[i].Imagin * -1) + "i";
                }
                else
                    s = Convert.ToString(a[i].Imagin) + "i";
                Console.WriteLine(s);
            }
            
            Console.ReadLine();
        }
    }
}
