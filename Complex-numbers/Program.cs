﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Complex_numbers
{
    public partial class Program
    {
        static void Main(string[] args)
        {
            int menu = 0;
            do
            {
                Console.Clear();
                Console.WriteLine("1. Сложение комплексных чисел");
                Console.WriteLine("2. Вычитание комплексных чисел");
                Console.WriteLine("3. Уможение комплексных чисел");
                Console.WriteLine("4. Деление комплексных чисел");
                Console.WriteLine("5. Возведение числа в степень");
                Console.WriteLine("6. Извлечение корня из числа");
                Console.WriteLine("7. Сравнение комплексных чисел");
                Console.WriteLine("8. Перевод числа из алгебрарической формы в тригонометрическую");
                Console.WriteLine("0. Выход");

                menu = Convert.ToInt32(Console.ReadLine());

                switch (menu)
                {
                    case 1:
                    case 2:
                    case 3:
                    case 4:
                    case 7:
                        Console.Clear();
                        Console.WriteLine("Введите первое число");
                        Complex a = EnteredValue();
                        Console.WriteLine("Введите второе число");
                        Complex b = EnteredValue();

                        Console.WriteLine("\nРезультат: ");
                        switch (menu)
                        {
                            case 1:
                                PrintComplex(SumComplex(a, b));
                                break;
                            case 2:
                                PrintComplex(DifComplex(a, b));
                                break;
                            case 3:
                                PrintComplex(MultiplyingComplex(a, b));
                                break;
                            case 4:
                                if (b.Imagin == 0 && b.Real == 0)
                                {
                                    Console.WriteLine("Делитель не может быть равен нулю");
                                    Console.ReadLine();
                                }
                                else
                                {
                                    Console.WriteLine("\nРезультат: ");
                                    PrintComplex(DividingComplex(a, b));
                                }
                                break;
                            case 7:
                                if (AreEqualComplexNumbers(a, b))
                                    Console.WriteLine("Равны");                                    
                                else
                                    Console.WriteLine("Не равны");
                                Console.ReadLine();
                                break;
                        }
                        break;                   

                    case 5:
                    case 6:
                        Console.Clear();
                        Console.WriteLine("Введите число");
                        a = EnteredValue();
                        Console.WriteLine("Введите степень");
                        int deg = Convert.ToInt32(Console.ReadLine());

                        Console.WriteLine("\nРезультат: ");
                        switch (menu)
                        {
                            case 5:
                                if (deg == 0)
                                    Console.WriteLine("0");
                                else
                                    PrintComplex(PowComplex(a, deg));
                                break;
                            case 6:
                                Complex[] res = RootComplex(a, deg);  
                                PrintComplex(res, deg);        
                                break;
                            default:
                                break;
                        }
                        break;
                    case 8:
                        Console.Clear();
                        Console.WriteLine("Введите число");
                        a = EnteredValue();
                        Console.WriteLine(AlgebToTrigComplex(a));
                        Console.ReadLine();
                        break;
                    case 0:
                        Environment.Exit(0);
                        break;
                    default:
                        break;
                }
            } while (menu != 0);

        }
    }
}
