﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Complex_numbers;
using System.Collections.Generic;

namespace Complex_numbers_Test
{
    [TestClass]
    public class UnitTest1
    {
        [TestMethod]
        public void AreEqualComplexNumbers_True()
        {            
            Complex a = new Complex(1, 1);
            Complex b = new Complex(1, 1);
            Assert.AreEqual(true, Program.AreEqualComplexNumbers(a, b));

            a.Real = -1;
            b.Real = -1;
            a.Imagin = 0;
            b.Imagin = 0;
            Assert.AreEqual(true, Program.AreEqualComplexNumbers(a, b));
        }

        [TestMethod]
        public void AreEqualComplexNumbers_False()
        {
            Complex a = new Complex(1, 1);
            Complex b = new Complex(1, 2);
            Assert.AreNotEqual(true, Program.AreEqualComplexNumbers(a, b));
        }         

        [TestMethod]
        public void SumComplex_Result()
        {
            Complex a = new Complex(3, -2);
            Complex b = new Complex(5, 2);
            Complex res = Program.SumComplex(a, b);
            Complex trueRes = new Complex(8, 0);
            Assert.AreEqual(res.Real, trueRes.Real);
            Assert.AreEqual(res.Imagin, trueRes.Imagin);

            b = new Complex(0, 0);
            a = new Complex(0, 0);
            res = Program.SumComplex(a, b);
            Assert.AreEqual(res.Imagin, 0);
            Assert.AreEqual(res.Real, 0);
        }

        [TestMethod]
        public void DifComplex_Result()
        {
            Complex a = new Complex(3, -2);
            Complex b = new Complex(5, 2);
            Complex res = Program.DifComplex(a, b);
            Complex trueRes = new Complex(-2, -4);
            Assert.AreEqual(res.Real, trueRes.Real);
            Assert.AreEqual(res.Imagin, trueRes.Imagin);

            b = new Complex(0, 0);
            a = new Complex(0, 0);
            res = Program.SumComplex(a, b);
            Assert.AreEqual(res.Imagin, 0);
            Assert.AreEqual(res.Real, 0);
        }

        [TestMethod]
        public void MultiplyingComplex_Result()
        {
            Complex a = new Complex(1, -2);
            Complex b = new Complex(3, 4);
            Complex res = Program.MultiplyingComplex(a, b);
            Complex trueRes = new Complex(11, -2);
            Assert.AreEqual(res.Real, trueRes.Real);
            Assert.AreEqual(res.Imagin, trueRes.Imagin);

            a = new Complex(0, -2);
            b = new Complex(0, 3);
            res = Program.MultiplyingComplex(a, b);
            Assert.AreEqual(res.Real, 6);
            Assert.AreEqual(res.Imagin, 0);

            a = new Complex(-5.5, -2.3);
            b = new Complex(-3.2, -3.25);
            res = Program.MultiplyingComplex(a, b);
            Assert.AreEqual(res.Real, 10.125, 0.05);
            Assert.AreEqual(res.Imagin, 25.235, 0.05);
        }

        [TestMethod]
        public void PowComplex_Result()
        {
            Complex a = new Complex(2, 1);
            int deg = 5;
            Complex res = Program.PowComplex(a, deg);
            Complex trueRes = new Complex(-38, 41);
            Assert.AreEqual(res.Real, trueRes.Real);
            Assert.AreEqual(res.Imagin, trueRes.Imagin);

            a = new Complex(-2, -2);
            deg = 4;
            res = Program.PowComplex(a, deg);
            Assert.AreEqual(res.Real, -64);
            Assert.AreEqual(res.Imagin, 0);
            a = new Complex(-2, -2);
            deg = 1;
            res = Program.PowComplex(a, deg);
            Assert.AreEqual(res.Real, -2);
            Assert.AreEqual(res.Imagin, -2);
        }

        [TestMethod]
        public void RootComplex_Result()
        {
            List<Complex> list = new List<Complex>();
            list.Add(new Complex(2, -1));
            list.Add(new Complex(-2, 1));

            Complex a = new Complex(3, -4);

            int deg = 2;

            Complex[] res = Program.RootComplex(a, deg);
            Complex[] trueRes = new Complex[2] { new Complex(2, -1), new Complex(-2, 1) };
            
            for (int i = 0; i < deg; i++)
            {
                Assert.AreEqual(res[i].Real, trueRes[i].Real, 0.1);
                Assert.AreEqual(res[i].Imagin, trueRes[i].Imagin, 0.1);
            }


            a = new Complex(-2, -2);

            deg = 5;

            res = Program.RootComplex(a, deg);
            trueRes = new Complex[5] { new Complex(1.097, -0.559), new Complex(0.87,0.87), new Complex(-0.559,1.097),
                new Complex(-1.216, -0.193), new Complex(-0.193, -1.216) };

            for (int i = 0; i < deg; i++)
            {
                Assert.AreEqual(res[i].Real, trueRes[i].Real, 0.1);
                Assert.AreEqual(res[i].Imagin, trueRes[i].Imagin, 0.1);
            }
        }
                    

        [TestMethod]
        public void DividingComplex_Result()
        {
            Complex a = new Complex(-2, 1);
            Complex b = new Complex(1, -1);
            Complex res = Program.DividingComplex(a, b);
            Complex trueRes = new Complex(-1.5, -0.5);
            Assert.AreEqual(res.Real, trueRes.Real);
            Assert.AreEqual(res.Imagin, trueRes.Imagin);


            a = new Complex(-2, -1);
            b = new Complex(4, 0);
            res = Program.DividingComplex(a, b);
            trueRes = new Complex(-0.5, -0.25);
            Assert.AreEqual(res.Real, trueRes.Real);
            Assert.AreEqual(res.Imagin, trueRes.Imagin);

            a = new Complex(0, -1);
            b = new Complex(4, 0);
            res = Program.DividingComplex(a, b);
            trueRes = new Complex(0, -0.25);
            Assert.AreEqual(res.Real, trueRes.Real);
            Assert.AreEqual(res.Imagin, trueRes.Imagin);

            a = new Complex(-2, -1);
            b = new Complex(0, -4);
            res = Program.DividingComplex(a, b);
            trueRes = new Complex(-0.25, 0.5);
            Assert.AreEqual(res.Real, trueRes.Real);
            Assert.AreEqual(res.Imagin, trueRes.Imagin);
            
             a = new Complex(-2, 1);
             b = new Complex(0, 4);
             res = Program.DividingComplex(a, b);
             trueRes = new Complex(-0.25, -0.5);
             Assert.AreEqual(res.Real, trueRes.Real);
             Assert.AreEqual(res.Imagin, trueRes.Imagin);

             a = new Complex(2, 1);
             b = new Complex(0, 4);
             res = Program.DividingComplex(a, b);
             trueRes = new Complex(-0.25, 0.5);
             Assert.AreEqual(res.Real, trueRes.Real);
             Assert.AreEqual(res.Imagin, trueRes.Imagin);
        }                 
    }
}
